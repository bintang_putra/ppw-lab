from django.shortcuts import render
from django.http import HttpResponseRedirect, Http404
from .forms import Todo_Form
from .models import Todo

# Create your views here.
response = {}
def index(request):    
    response['author'] = "Bintang Ilham Syahputra" 
    todo = Todo.objects.all()
    response['todo'] = todo
    html = 'lab_5/lab_5.html'
    response['message'] = ''
    if ('new_todo' in request.GET.dict().keys()):
        if (request.GET.dict()['new_todo'] == 'true'):
            response['message'] = 'Berhasil menambahkan Todo'
    response['todo_form'] = Todo_Form
    return render(request, html, response)

def add_todo(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['title'] = request.POST['title']
        response['description'] = request.POST['description']
        todo = Todo(title=response['title'],description=response['description'])
        todo.save()
        return HttpResponseRedirect('/lab-5/?new_todo=true')
    else:
        return HttpResponseRedirect('/lab-5/')

def delete_todo(request, id):
    if (len(Todo.objects.filter(id=id).all()) == 0):
        raise Http404
    else:
        Todo.objects.all().delete()
        return HttpResponseRedirect('/lab-5/')
