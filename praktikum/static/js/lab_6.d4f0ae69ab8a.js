/**
Lab 6
dibuat oleh Bintang Ilham Syahputra
SI 2016, 1606825902
*/

var print;      // Variable untuk menampilkan hasil output calculator
var chatBox;    // Variable untuk menampilkan hasil input di chatbox
var chat;       // Variable untuk menampilkan hasil output chatbox
var calculator; // Variable untuk menampilkan hasil output kalkulator
var calculatorError = false;
var chatHead;
var dropdownTheme;
var navbar;
var navbarBrand;

const ENTER = 13;
const themes = [
  {id:0,text:'Red',bcgColor:'#F44336',fontColor:'#FAFAFA'},
  {id:1,text:'Pink',bcgColor:'#E91E63',fontColor:'#FAFAFA'},
  {id:2,text:'Purple',bcgColor:'#9C27B0',fontColor:'#FAFAFA'},
  {id:3,text:'Indigo',bcgColor:'#3F51B5',fontColor:'#FAFAFA'},
  {id:4,text:'Blue',bcgColor:'#2196F3',fontColor:'#212121'},
  {id:5,text:'Teal',bcgColor:'#009688',fontColor:'#212121'},
  {id:6,text:'Lime',bcgColor:'#CDDC39',fontColor:'#212121'},
  {id:7,text:'Yellow',bcgColor:'#FFEB3B',fontColor:'#212121'},
  {id:8,text:'Amber',bcgColor:'#FFC107',fontColor:'#212121'},
  {id:9,text:'Orange',bcgColor:'#FF5722',fontColor:'#212121'},
  {id:10,text:'Brown',bcgColor:'#795548',fontColor:'#FAFAFA'}
];
const defaultTheme = {id:0,text:'Indigo',bcgColor:'#3F51B5',fontColor:'#FAFAFA'};

/**
* Mengirim pesan dan merender
* @param {*} event
*/

const sendMessage = function(event){
  if(event.which == ENTER){       // Jika event user === ENTER(13)
    chat.append(createMessage(true, chatBox.val()));  // Hasil pesan ditampilkan dalam chatBox
    chatBox.val('');    // Mengembalikan chatArea menjadi kosong
  }
};

/**
* Membuat pesan dan respon
* Jika pesan,maka html berwarna putih
* Jika respone, maka html berwarna hijau muda
* @param {*} isSender ==> menentukan apakah dia sender, atau receiver
* @param {*} message ==> pesan yang telah ditulis
*/
const createMessage = function(isSender, message){
  const style = isSender ? 'msg-send':'msg-receive';
  return '<div class ="'+style+'">'+ message +'</div>';
};

/**
* Memilih tema dari dropdown
*/
const selectTheme = function(){
  const selected = dropdownTheme.select2('data')[0]; // Mengambil data yang telah dimasukan kedalam queryset
  const theme = {
    id = selected.id;
    text = selected.text;
    bcgColor = selected.bcgColor;
    fontColor = selected.fontColor;
  };
  localStorage.selectedTheme = JSON.stringify(theme);
  ApplyTheme(theme);
};

/**
* Apply tema yang telah dipilih
* @param {*} theme ===> tema yang telah dipilih
*/
const ApplyTheme = function(theme){
  calculator.css('background-color', theme.bcgColor);
  calculator.find('#title').css('color', theme.fontColor);
  chatHead.css('background-color', theme.bcgColor);
  chatHead.find('h2').css('color',theme.fontColor);
  navbar.css('background-color', theme.bcgColor);
  navbarBrand.css('color', theme.fontColor);
};

/**
*   inisiasi Variable yang telah dideklarasikan
*   Jalankan jQuery ketika documen telah berhasil di load
*/
$(document).ready(() =>{
  initLocalStorage();     // Membuat localStorage awal
  print = $('#print');
  chat = $('.msg-insert');
  chatBox = $('#chat-textarea');
  calculator = $('.model');
  chatHead = $('.chat-head');
  dropdownTheme = $('#my-select');
  navbar = $('.navbar');
  navbarBrand = $('.navbar-brand');
  chatBox.keypress(sendMessage);    // Event keypress, maka pesan terkirim
  dropdownTheme.select2({
    'data': JSON.parse(localStorage.themes)   // Memilih dan menyimpan hasil kedalam 'data'
  });
  $('#apply').on('click', selectTheme);   // Jika user klik, maka method selectTheme akan terpanggil
  ApplyTheme(JSON.parse(localStorage.selectedTheme)); // Mengubah tema berdasarkan hasil dari method selectedTheme
  dropdownTheme.val(JSON.parse(localStorage.selectedTheme).id); // Set id dropdownTheme menjadi id theme yang terpilih
  dropdownTheme.trigger('change');
  unitTest(); //QUnit test
});

/**
* Membuat localStorage baru jika piliham tema belum ada
*/
const initLocalStorage = function(){
  if(!localStorage.themes){     // Cek apakah temanya udah ada atau belum
    localStorage.themes = JSON.stringify(themes);   // Masukan list tema kedalam localStorage
    localStorage.selectedTheme = JSON.stringify(defaultTheme);  // Set defaultTheme
  }
};

/**
* Operasi dalam kalkulator
*/
const go = command =>{    // Jika kalkulator menghasilkan event klik (onclick), maka go di set ke command
  if(command === 'ac'){
    print.val('');
  }
  else if(command === 'eval'){  // Jika command === '=' pada html
    var result;
    try{
      result = Math.round(evaluate(print.val()) * 10000) /10000;
    }
    catch(error){
      result = 'Syntax error';
      calculatorError = true;
    }
    if(result == Infinity || isNaN(result)) result = 'undefined';
    print.val(result);
  }
  else{
    if(calculatorError){
      print.val(command);
      calculatorError = false;
    }
    else {
      print.val(print.val() + command);
    }
  }
};

function evaluate(fn){
  return new Function('return ' + fn)();
}
