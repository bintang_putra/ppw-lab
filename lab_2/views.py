from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'Menjadi bagian dari Civitas Universitas Indonesia sejak 2016. Sedang berusaha mempelajari hal-hal yang berbau teknologi, supaya terlihat nyata seorang mahasiswa FASILKOM-nya. Maklum, setahun terakhir fokus menjeramah dunia UI secara keseluruhan'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
