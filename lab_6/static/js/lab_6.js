/*
* Lab_6. js
*	Dibuat oleh Bintang Ilham Syahputra
* SI 2016, 1606825902
*/
var counter = 0;	// kebutuhan warna
var message = "";		// Variabel untuk menyimpan input user
var chatPanel = document.getElementById("chat_textarea"); // Tempat untuk menulis pesan
var chat = function(event){
	if(event.keyCode === 13 && counter%2==0){	// Pesan dari sender
		chatPanel.value = "";		// Set tulisan input user menghilang
		var messageBox = document.createElement("P"); // membuat elemen paragraf
		var fillMessage = document.createTextNode(message);	// membuat pesan yang ditulis oleh user
		var box = document.getElementsByClassName("msg-insert")[0];
		messageBox.setAttribute("class","msg-send");
		messageBox.appendChild(fillMessage); // Memasukan pesan kedalam paragraf
		box.appendChild(messageBox);	// Memasukan elemen paragraf kedalam div class "msg-insert"
		message = "";
		counter = counter+1;
	}
	else if(event.keyCode === 13 && counter%2!=0){ // Pesan dari receiver
		var messageBox = document.createElement("P"); // membuat elemen paragraf
		var fillMessage = document.createTextNode(message);	// membuat pesan yang ditulis oleh user
		var box = document.getElementsByClassName("msg-insert")[0];
		messageBox.setAttribute("class","msg-receive");	// membuat class attribute dengan nama "msg-receive"
		messageBox.appendChild(fillMessage); // Memasukan pesan kedalam paragraf
		box.appendChild(messageBox);	// Memasukan elemen paragraf kedalam div class "msg-insert"
		message = "";
		counter = counter+1;
	}
	else{
		message+=event.key;
		console.log(message);
	}
}

// Calculator function
var print = document.getElementById('print');
var erase = false;
var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
    erase = true;
  } else if (x === 'eval') {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
        erase = true;
  } else if (x === 'log') {
				// Mengkonversi input user menjadi derajat
				// Fungsi log10 sudah ada di module Math
        print.value = Math.log10(print.value);
  } else if (x === 'sin') {
				// Mengkonversi input user menjadi derajat
				// Fungsi Sin sudah ada di module Math
        print.value = Math.sin(print.value * Math.PI / 180); //dalam derajat
        if(print.value == 1.2246467991473532e-16) print.value = 0; // Jika input == Pi, return 0 karena infinite number decimal
  } else if (x === 'tan') {
		// Mengkonversi input user menjadi derajat
		// Fungsi tan sudah ada di module Math
        print.value = Math.tan(print.value * Math.PI / 180); //dalam derajat
        if(print.value == -1.2246467991473532e-16) print.value = 0;	// Jika input == Pi , return 0 karena infinite number decimal
        else if(print.value == 0.9999999999999999) print.value = 1;	// Jika input hampir mendekati 1
  } else {
        print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

var theme = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

//var tema = JSON.parse(theme);	// Mengubah string theme menjadi object javascript tema agar bisa di retrieve
// localStorage.setItem('themes',JSON.stringify(theme)); // memasukan list theme kedalam 'themes'
// if(localStorage.selectedTheme == null){
//	localStorage.setItem('selectedTheme',JSON.stringify(theme[3]));	// Default tema adalah Indigo
	//localStorage.setItem('selectedTheme',theme[3]);	// Default tema adalah Indigo
//}
//$("body").css('background-color',JSON.parse(localStorage.getItem('selectedTheme')).bcgColor);	// Mengambil elemen body, lalu mengubah style background color menjadi bcgColor selectedTheme
//$("text-center").css('color',JSON.parse(localStorage.getItem('selectedTheme')).fontColor);	// Mengubah tulisan

$(document).ready(function() {
    $('.my-select').select2({
			// Nampilinin pilihan warna
    	//'data':JSON.parse(localStorage.getItem('themes'))
			'data':theme
    });
});

$('.apply-button-class').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var selectedId = $(".my-select").val();
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
	localStorage.setItem('selectedTheme',JSON.stringify(theme[selectedId]));
    // [TODO] ambil object theme yang dipilih
    var choices = localStorage.getItem('selectedTheme');

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    console.log(JSON.parse(localStorage.getItem('selectedTheme')));
    $("body").css('background-color',JSON.parse(localStorage.getItem('selectedTheme')).bcgColor);
    $(".text-center").css('color',JSON.parse(localStorage.getItem('selectedTheme')).fontColor);
    // [TODO] simpan object theme tadi ke local storage selectedTheme
})
