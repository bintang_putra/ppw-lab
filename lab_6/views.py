from django.views.generic import TemplateView
from django.shortcuts import render

# Create your views here.
response = {}
def index(request):
    html = 'lab_6/lab_6.html'
    response['author'] = 'Bintang Ilham Syahputra'
    return render(request, html, response)
