# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-17 07:34
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lab_7', '0006_auto_20171217_1325'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='friend',
            name='angkatan',
        ),
    ]
